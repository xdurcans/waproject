<?php

class Messages extends Model {

    function getAllByRoomId($roomId, $userId) {
        $stmt = $this->db->prepare('SELECT users.login, messages.* FROM users JOIN messages ON messages.id_users_from= users.id_users WHERE id_rooms = :id AND (id_users_to = :idU OR id_users_to IS NULL OR id_users_from = :idU ) ORDER BY created');
        $stmt->bindValue(':id', $roomId);
        $stmt->bindValue(':idU', $userId);
        $stmt->execute();
        return $stmt->fetchAll();
    }

    function sendMessage($roomId, $msg, $idUs, $idTo) {
        $stmt = $this->db->prepare('INSERT INTO messages (id_rooms, id_users_from,id_users_to, created, message) VALUES (:idR, :idUs,:idTo , NOW(), :msg)');
        $stmt->bindValue(':msg', $msg);
        $stmt->bindValue(':idUs', $idUs);
        $stmt->bindValue(':idTo', $idTo);
        $stmt->bindValue(':idR', $roomId);
        return $stmt->execute();

    }

}
