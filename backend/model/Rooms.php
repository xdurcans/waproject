<?php

class Rooms extends Model {

    function add($title, $idOwner, $lock = 0) {
        $stmt = $this->db->prepare('INSERT INTO rooms '
                . '(created, title, id_users_owner, lock)'
                . ' VALUES '
                . '(NOW(), :t, :id, :l)');
        $stmt->bindValue(':t', $title);
        $stmt->bindValue(':id', $idOwner);
        $stmt->bindValue(':l', $lock);
        return $stmt->execute();
    }

    function all() {
        $stmt = $this->db->query('SELECT * FROM rooms ORDER BY created');
        return $stmt->fetchAll();
    }

    function find($id) {
        $stmt = $this->db->prepare('SELECT * FROM rooms WHERE id_rooms = :id');
        $stmt->bindValue(':id', $id);
        $stmt->execute();
        return $stmt->fetch();
    }

    function delete($id) {
        $stmt = $this->db->prepare('DELETE FROM rooms WHERE id_rooms = :id');
        $stmt->bindValue(':id', $id);
        $stmt->execute();
        return $stmt->fetch();
    }

    function lock($id){
        $stmt = $this->db->prepare('UPDATE rooms SET lock = true WHERE id_rooms = :id');
        $stmt->bindValue(':id', $id);
        $stmt->execute();
        return $stmt->fetch();
    }

    function unlock($id){
        $stmt = $this->db->prepare('UPDATE rooms SET lock = false WHERE id_rooms = :id');
        $stmt->bindValue(':id', $id);
        $stmt->execute();
        return $stmt->fetch();
    }

    function enterRoom($id_room, $id_user){
        $stmt = $this->db->prepare('INSERT INTO  in_room (id_users, id_rooms, last_message, entered) VALUES (:idU, :idR, NOW(), NOW()) ');
        $stmt->bindValue(':idR', $id_room);
        $stmt->bindValue(':idU', $id_user);
        return $stmt->execute();
    }

    function leaveRoom($id_room, $id_user){
        $stmt = $this->db->prepare('DELETE FROM in_room WHERE id_users = :idU AND id_rooms = :idR');
        $stmt->bindValue(':idR', $id_room);
        $stmt->bindValue(':idU', $id_user);
        return $stmt->execute();
    }

    function getAllInRoom($id, $idU){
        $stmt = $this->db->prepare('SELECT in_room.*, users.login FROM users JOIN in_room ON in_room.id_users = users.id_users WHERE in_room.id_rooms = :id AND users.id_users <> :idU');
        $stmt->bindValue(':id', $id);
        $stmt->bindValue(':idU', $idU);
        $stmt->execute();
        return $stmt->fetchAll();
    }


}
